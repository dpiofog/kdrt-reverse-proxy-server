#syntax=docker/dockerfile:1
FROM python:3.7

WORKDIR /gitlab_reverse_proxy

RUN apt-get update && apt-get install -y python3-opencv

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . .

CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0"]

