
from datetime import datetime
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("dpi-ofog-firebase-adminsdk-kev7o-938aece3b3.json")
firebase_admin.initialize_app(cred, {
    'storageBucket': 'dpi-ofog.appspot.com'
})



db = firestore.client()
lastHistoryId = ""




testObjects = {"test things 1":"updag"}
testObjects1 = {"test things 1":"2ndupdating"}
# data = {
#     "history_id": "2",
#     "user_id": "testID",
#     "date": dt_string,
#     "link": "test link",
#     "objects": testObjects,
#     "duration": 80,
#     "is_delete": False,

# }

# Function to upload history to firebase 
def upload_history (user_id:str, link:str, detectedObjects:dict, duration:str, timestamp, device_id):
    global lastHistoryId
    # now = datetime.now()
    # timestamp = datetime.timestamp(now)
    data = {
    "user_id": user_id,
    "date": timestamp,
    "link": link,
    "objects": detectedObjects,
    "duration": duration,
    "device_id": device_id,
    "is_delete": False,
            }
    doc_ref = db.collection("history").add(data)
    return doc_ref[1].id

# Function to get user_id given the device_id
def getUserId(device_id:str):
    doc_ref = db.collection("devices").document(device_id)
    doc = doc_ref.get()
    if doc.exists:
        print(f'Document data: {doc.to_dict()}')
        return doc.to_dict().get("user_id")
    else:
        return "none"

# Function to update detected object after object detection done
def updateDetectedObject(historyId, detectedObjects):
    doc_ref = db.collection("history").document(historyId)
    doc_ref.update({'objects':detectedObjects})


# Function to parse the device id and timestamp with the agreed filename
def get_deviceId_timestamp(file_name):
    splitted = file_name.split("_",1)
    deviceId = splitted[0]
    messy = splitted[1][:-4]
    date_time_obj = datetime.strptime(messy, '%Y-%m-%d_%H_%M_%S.%f')
    timestamp = datetime.timestamp(date_time_obj)
    return deviceId, timestamp


    
    

# result = firebase.get('/dpi-ofog-default-rtdb/History', None)
# # result = firebase.post("/dpi-ofog-default-rtdb/History", data)
# upload_history("yo", "test link", testObjects, 90)
# updateDetectedObject(testObjects1)
# print(len(result))