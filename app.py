from logging import exception
from flask import Flask,request,redirect,Response
from werkzeug.utils import secure_filename
from flask_restx import Api, Resource
from werkzeug.datastructures import FileStorage
from fbinsert import *
from firestorage import *
import requests, os
import cv2
import datetime
app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 75 * 1024 * 1024
ALLOWED_EXTENSIONS = {'mp4', 'mov', 'wmv', 'avi', 'mkv','wav'}

api = Api(app, version = "1.0.6", 
        title= "Angelica Reverse Proxy Documentation",
        description = "Upload video to be processed and identify",
         )


ai_name_space = api.namespace('api-detection', descriptions='Manage API for Object Detection AI')

upload_parser = api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)

# Change this to a real website name

OBJECT_DETECTION_SITE_NAME = 'http://object-detection-load-balancer-128596440.ap-southeast-1.elb.amazonaws.com'
AUDIO_DETECTION_SITE_NAME = 'http://sound-ai-load-balancer-791860471.ap-southeast-1.elb.amazonaws.com'
# Checking allowed file extension
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Get video duration 
def get_video_duration(filename):
    video = cv2.VideoCapture(filename)

    frames = video.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = int(video.get(cv2.CAP_PROP_FPS))
    
    # calculate duration of the video
    seconds = int(frames / fps)
    video_time = str(datetime.timedelta(seconds=seconds))
    
    return video_time

def deleteFile(file_name):
    os.remove("./{video_name}".format(video_name=file_name))
    



@ai_name_space.route('/video/predict/<string:device_id>')
@api.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Network Error' })

@api.expect(upload_parser)
class Predict(Resource):
    def post(self,device_id):
        global OBJECT_DETECTION_SITE_NAME
        args = upload_parser.parse_args()
        video = args['file']
       
        user_id = getUserId(device_id)

        if user_id == "none":
            ai_name_space.abort(400, status= "Devices Id not found", statuscode= "400")
        
        elif user_id == "" or user_id == None:
            ai_name_space.abort(400, status= "There's no user id connected to device", statuscode= "400")


        
        
        
            

        if video and allowed_file(video.filename):

            file_name = secure_filename(video.filename)

            print(file_name)


            try:
                print(file_name)
                temp_id_timestamp =get_deviceId_timestamp(file_name)
                print(temp_id_timestamp)
                timestamp = temp_id_timestamp[1]
        
        
            except Exception as e:
                print (e)
                ai_name_space.abort(400, status= "File name not supported", statuscode= "400")
        
        


            # Saving video file
            video.save(video.filename)
            # Get the video filename 
            file_name = video.filename

            
            # Get video duration
            duration = get_video_duration(file_name)

            # open the video file to be processed to object detection AI
            received_file = open(f"{file_name}", "rb")

            # upload video cloud storage
            link = uploadVideos(file_name)

            # detected_objects_dict = resp.json() 
            detected_objects_dict = {}

            # upload history data to fire storage
            historyId = upload_history(user_id, link,detected_objects_dict,duration,timestamp,device_id)

            # Forward the request to backend
            try:
                resp = requests.post(f'{OBJECT_DETECTION_SITE_NAME}/predict', files={"file" : received_file} )            
            
            except Exception as e:
                received_file.close()
                os.remove("./{video_name}".format(video_name=file_name))
                detected_objects_dict = {'person':1}
                updateDetectedObject(historyId, detected_objects_dict)
                ai_name_space.abort(500,  e.__doc__ ,status= "Object Detection May be Down", statuscode= "500")

            response = Response(resp.content, resp.status_code)
            # Saving the detected object to dictionary, will be processed to database (change) (exit the exception)
            detected_objects_dict = resp.json().get("result")


            
            updateDetectedObject(historyId, detected_objects_dict)
             # Close the video file and delete the video 
            received_file.close()
            os.remove("./{video_name}".format(video_name=file_name))

            print(resp.json())
            return response

        else:  return "File size must not be greater than 75mb and extensions allowed: {ALLOWED_EXTENSIONS}"

@ai_name_space.route('/audio/predict')
@api.expect(upload_parser)
class Predict(Resource):
    def post(self):
        global AUDIO_DETECTION_SITE_NAME
        args = upload_parser.parse_args()
        audio = args['file']
        try:
            resp = requests.post(f'{AUDIO_DETECTION_SITE_NAME}/audio/predict', files={"file" : audio})
            if(resp.status_code != 200):
                raise Exception("Unable to process the request")
        except Exception as e:
            ai_name_space.abort(500,  e.doc ,status= "Audio Detection", statuscode= "500")
        response = resp.json()
        return response
        

  
if __name__ == '__main__':
    app.run(host="0.0.0.0",debug = False)
