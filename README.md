<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url] -->
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/dpiofog/kdrt-frontend">
    <img src="./screenshots/logo.PNG" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Angelica</h3>

  <p align="center">
    A companion app to help victims of domestic violence
    <!-- <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a> -->
    <!-- <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a> -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <!-- <li><a href="#roadmap">Roadmap</a></li> -->
    <!-- <li><a href="#contributing">Contributing</a></li> -->
    <!-- <li><a href="#license">License</a></li> -->
    <li><a href="#members">Members</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Domestic violence or KDRT in Indonesian is defined as an act of violence physically or mentally to the victim by aggression among people with kinship. The victim in the family could be anyone such as children, women, men, or the elderly. Domestic violence issues can range from minor impact like humiliation to concerning impact such as sexual abusement (Holt, 2008). In reality, it doesn't matter if it's only small things like humiliation, it still gives a bad impact to the victim and it won’t guarantee it will not escalate to the next level. In the end the victim will be left with posttraumatic stress and insecurity that could be carried for the rest of their life. 

Due to the COVID-19 pandemic resulting in some countries conducting lockdown for their citizens, there has been an increase of reports on domestic violence. For example, there has been an increase of 40% in reported domestic violence in Brazil (Bradbury-Jones, 2020). This incident happens because citizens need to battle the pandemic, which in turn isolates them alongside their abusers, increasing the frequency of experiencing domestic violence  (Kofman, 2020). A big concern is when the victims become reluctant or unable to show evidence when reporting the acts of violence that have befallen them.

Our proposed solution, as to be our project, is a device that would be capable of discreetly collecting evidence of domestic violence by way of audio and video recording. In order to minimize misuse of the device and save storage space, it will only start recording and storing the evidence if it detects the signs of domestic violence. The microphone will act as a sensor for audio queues where an AI will be used to determine if the sounds that it picks up is associated with domestic violence. If it does, the camera will be turned on and record the whole situation. The camera will also be equipped with an AI that can detect objects that it sees as to provide additional evidence. All recordings will be stored in cloud storage and the device itself will only store the latest recordings if it does not have enough space.

<div align="right">(<a href="#top">back to top</a>)</div>



### Built With

This repository holds the backedn reverse-proxy section of the whole project. It is a reverse-proxy api that was created using [Python](https://www.python.org/downloads/release/python-370/).

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- GETTING STARTED -->
## Getting Started

If you would like to try out the finished application, you can build the docker image from Dockerfile. [Docker](https://www.docker.com/products/docker-desktop) needs to be installed. Run the following command in the terminal.
1. Building Docker image
    ```sh
    docker build --tag reverse-proxy . 
    ```
2. Run docker image in a container 
    ```sh
    docker run -d -p 5000:5001 reverse-proxy
    ```
3. Type in webrowser 
    localhost:5000/

### Prerequisites

[Python](https://www.python.org/downloads/release/python-370/) and [PIP](https://www.python.org/downloads/release/python-370/) needs to be installed, alongside a choice of your preferred IDE. 

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/dpiofog/kdrt-backend-reverse-proxy.git
   ```
2. Open `kdrt-backend-object-detection` project using your IDE.
3. Installing python necessary library
   ```sh
   pip install -r requirements.txt 
   ```
4. Run the app.py 
   ```sh
   python app.py
   ```
   This will launch object detection to your localhost:5000.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

The purpose of this program is to detect the videos recorded by your [Angelica's Raspberry Pi](https://gitlab.com/dpiofog/kdrt-raspberrypi). It will return the detected the quantity of the detected objects in json. 

When you first open the app, you will be greated with the landing page. From here, you need to either register a new account or sign in.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/home.PNG" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Swagger home screen</i></p> </td>
    </tr>
  </table>
</div>

If this is your first time using the swagger, you will be greeted with an empty home screen with collapsing feature. Tap on the big down arrow icon to collapse the API. 

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/collapse.png" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Swagger API</i></p> </td>
    </tr>
  </table>
</div>




OBJECT DETECTION API

You can click browse to put in the video that you want to detect

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/browse.jpg" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Try it out clicked</i></p> </td>
    </tr>
  </table>
</div>
You can click browse to put in the video that you want to detect. The file name of the video should be 
"deviceID Year-Month-Day Hour_Minute_Second.Nanoseconds"
Or it will return false 
The video format should be {'mp4', 'mov', 'wmv', 'avi', 'mkv'} or it will not accept it 
You also need to input the registered device_id to able use it 


<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/input.PNG.jpg" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Inputting video file and device id</i></p> </td>
    </tr>
  </table>
</div>

You may click the execute to run the reverse proxy that connects to the object-detection backend.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/execute.jpg" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>execute clicked</i></p> </td>
    </tr>
  </table>
</div>

Wait for a while and the object detection will give a response. The result would be a json that have an identifier. The result would be uploaded to the firebase database with the proper device id  

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/result.jpg" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Result generated</i></p> </td>
    </tr>
  </table>
</div>


We hope that this api can be used to aid victims of domestic violence and abuse by detecting person and its object.



AUDIO DETECTION API

You can click browse to put in the audio that you want to detect



<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/screenshot1.png" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Selecting Audio</i></p> </td>
    </tr>
  </table>
</div>





The only requirements for the audio is that the audio has to be mono channel audio in the .wav format.  There is also a size limit for the audio where the filesize cannot be any larger than 2MB in size. 

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/screenshot3.png" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Audio selected</i></p> </td>
    </tr>
  </table>
</div>





After selecting a compatible audio file, you may execute to access the audio AI through the reverse proxy.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/screenshot4.png" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Executed</i></p> </td>
    </tr>
  </table>
</div>



Once executed, the AI will give its verdict on the content within the audio. The resulting response is a json object containing the status, status code and the verdict on whether the audio contains potential domestic violence.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/screenshot5.png" width=1500> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Result generated</i></p> </td>
    </tr>
  </table>
</div>



The use of this API shows that the sound AI is capable of detecting potential domestic violence in its surroundings allowing our device to perform its duties.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- MEMBERS -->
## Members

- [Gardyan Akbar](https://gitlab.com/giantsweetroll)
- [Eric Edgari](https://gitlab.com/Trutina1220)
- [Vincentius Gabriel](https://gitlab.com/kronmess)
- [Jocelyn Thiojaya](https://gitlab.com/jocelynthiojaya)

Project Link: [https://gitlab.com/dpiofog/](https://gitlab.com/dpiofog/)

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

_Python packages used in the making of the project_
* [flask](https://pypi.org/project/Flask/)
* [flask-restx](https://pypi.org/project/flask-restx/)
* [opencv-python](https://pypi.org/project/opencv-python/)

_Works cited_
* Bradbury‐Jones, C., & Isham, L. (2020). The pandemic paradox: The consequences of
	COVID‐19 on domestic violence. Journal of Clinical Nursing, 29(13–14), 2047–
	1.    [https://doi.org/10.1111/jocn.15296](https://doi.org/10.1111/jocn.15296)
	children and young people: A review of the literature. Child Abuse & Neglect, 32(8), 
	797–810. [https://doi.org/10.1016/j.chiabu.2008.02.004](https://doi.org/10.1016/j.chiabu.2008.02.004)
* Kofman, Y. B., & Garfin, D. R. (2020). Home is not always a haven: The domestic violence crisis amid the COVID-19 pandemic. Psychological Trauma: Theory, Research, Practice, and Policy, 12(S1), S199.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues -->
<!-- [license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge -->
<!-- [license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew -->
<!-- [product-screenshot]: images/screenshot.png -->
