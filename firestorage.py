import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage, initialize_app
from fbinsert import *

# cred = credentials.Certificate("dpi-ofog-firebase-adminsdk-kev7o-938aece3b3.json")
# firebase_admin.initialize_app(cred, )



def uploadVideos(file_name):
    bucket = storage.bucket()
    blob = bucket.blob(file_name)
    blob.upload_from_filename(file_name)
    blob.make_public()
    return blob.public_url


def deleteVideos(file_name):
    bucket = storage.bucket()
    blob = bucket.blob(file_name)
    blob.delete()
    
    return f"{file_name} is deleted"




